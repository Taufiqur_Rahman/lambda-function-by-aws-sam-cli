# --Create lambda function by invoking S3 bucket through AWS SAM CLI--

# Software Requirements 

To run the fucntion in local environment through AWS SAM CLI, it requires
  - AWS Account.
  - AWS Command Line Interface (CLI).
  - SAM CLI.
  - Docker.

# Installing AWS CLI using pip

  - Verify that the Python version is 2.7 or 3.6.
        $ python --version
  - Check pip version.
        $ pip --verison
  - Install AWS-SAM-CLI.
        $ pip install --user aws-sam-cli
  - Adjust the PATH to include the Python scripts that are installed under the user's home directory.
  - Check AWS-SAM-CLI is installed.
        $ sam --version
  - To Upgrade sam by using pip.
        $ pip install --user --upgrade aws-sam-cli
 

# Create Lambda Function using AWS SAM CLI

* To create the stacture of serverless application
        $ sam init --runtime python3.7
* To create an application in local environment
       $ sam init --runtime python3.7 --name application name
* Open the IDE to write lambda function code
* Then create a new folder in application folder and under of the folder create a python file.
* Now template.yaml file need to edit according to the lambda function
  (e.g. Resources: 
    GetEC2RegionsFunction:
        Type: AWS::Serverless::Functions
    Propertises:
        FunctionName: GetEc2Regions
        Descriptions: Sam Demo Get EC2 Regions
        Role: arn:aws:iam::093833860903:role/Mylambda (arn of IAM Role)
        codeuri: get-ec2-regions
        Handler: main.lambda_handler (Formate- handler: fileName.FunctionName)
        Runtime: python 3.7
    Environment:
        variables:
           PARA1: VALUE
        Events:
           GetEC2RegionsAPI:
              Type: API
            Propertises:
              path:/getec2regions


   Output:
   GetEC2RegionsAPI:-
     Description:"----------"
     Value: !Sub "https://${ServerlessRestApi}.execute-api.${AWS::Regions}.amazonaws.com/prod/getec2regions"
    GetEC2RegionsFunction:-
     Description:"----------"
     Value: !GetAtt GetEC2Regions.Arr
     )
     
# Test lambda function in API Gateway locally
1. To conduct the step 1 make sure Docker software is running.
    $sam local invoke functionname --no-event (e.g. functionname:getec2regions)
2. To run the lambda function on local server
    $sam local start-api
3. Then copy the URL with port number and paste in browser to see the result of lambda function

4. To store lambda function invoke S3 Bucket
    $S3 package --template-file template.yaml --output-template file filename.yaml --s3-bucket bucket name (e.g.filename.yaml- deploy.yaml; bucket name- create a bucket in S3 using AWS Console)





